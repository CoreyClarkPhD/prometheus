// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef PROMETHEUSPLUGIN_MyGameInstance_generated_h
#error "MyGameInstance.generated.h already included, missing '#pragma once' in MyGameInstance.h"
#endif
#define PROMETHEUSPLUGIN_MyGameInstance_generated_h

#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_29_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_13_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execReadJsonFileToDisplayBuffer) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->ReadJsonFileToDisplayBuffer(Z_Param_filename); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetListOfFilesAtTempLocation) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Files); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetListOfFilesAtTempLocation(Z_Param_Out_Files); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintEventBufferToFileIfExecutable) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintEventBufferToFileIfExecutable(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEventLocations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FVector>*)Z_Param__Result=this->GetEventLocations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentDisplayedEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetCurrentDisplayedEvent(Z_Param_eventName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoadedEventNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=this->GetLoadedEventNames(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execReadJsonFileToDisplayBuffer) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->ReadJsonFileToDisplayBuffer(Z_Param_filename); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetListOfFilesAtTempLocation) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Files); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetListOfFilesAtTempLocation(Z_Param_Out_Files); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintEventBufferToFileIfExecutable) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintEventBufferToFileIfExecutable(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEventLocations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FVector>*)Z_Param__Result=this->GetEventLocations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentDisplayedEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetCurrentDisplayedEvent(Z_Param_eventName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoadedEventNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=this->GetLoadedEventNames(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance(); \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance(); \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyGameInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public:


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMyGameInstance)


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_eventBuffer() { return STRUCT_OFFSET(UMyGameInstance, m_eventBuffer); }


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_48_PROLOG
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_RPC_WRAPPERS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_INCLASS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_INCLASS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
