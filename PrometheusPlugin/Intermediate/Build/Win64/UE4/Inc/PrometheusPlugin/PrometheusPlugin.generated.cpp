// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PrometheusPlugin.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1PrometheusPlugin() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API class UClass* Z_Construct_UClass_UGameInstance();

	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_CollectPositionalEvent();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetEventLocations();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_PrintEventBufferToFileIfExecutable();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_ReadJsonFileToDisplayBuffer();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance();
	PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	void UDataCollectionLibrary::StaticRegisterNativesUDataCollectionLibrary()
	{
		UClass* Class = UDataCollectionLibrary::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "CollectPositionalEvent", (Native)&UDataCollectionLibrary::execCollectPositionalEvent },
			{ "SetGameInstancePointer", (Native)&UDataCollectionLibrary::execSetGameInstancePointer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_CollectPositionalEvent()
	{
		struct DataCollectionLibrary_eventCollectPositionalEvent_Parms
		{
			FString eventType;
			FVector position;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CollectPositionalEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04822401, 65535, sizeof(DataCollectionLibrary_eventCollectPositionalEvent_Parms));
			UProperty* NewProp_position = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("position"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(position, DataCollectionLibrary_eventCollectPositionalEvent_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_eventType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventType"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventType, DataCollectionLibrary_eventCollectPositionalEvent_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("CollectPositionalEvent"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("Collect Positional Event"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Collect Positional Event"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Blueprint Functions"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer()
	{
		struct DataCollectionLibrary_eventSetGameInstancePointer_Parms
		{
			UMyGameInstance* gameInstance;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetGameInstancePointer"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(DataCollectionLibrary_eventSetGameInstancePointer_Parms));
			UProperty* NewProp_gameInstance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("gameInstance"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(gameInstance, DataCollectionLibrary_eventSetGameInstancePointer_Parms), 0x0010000000000080, Z_Construct_UClass_UMyGameInstance_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("Set Game Instance Pointer"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("Set Game Instance Pointer"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Set Game Instance Pointer"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataCollectionLibrary_NoRegister()
	{
		return UDataCollectionLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_UDataCollectionLibrary()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintFunctionLibrary();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = UDataCollectionLibrary::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000080;

				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_CollectPositionalEvent());
				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_CollectPositionalEvent(), "CollectPositionalEvent"); // 2491455869
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer(), "SetGameInstancePointer"); // 3649020208
				static TCppClassTypeInfo<TCppClassTypeTraits<UDataCollectionLibrary> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("DataCollectionLibrary.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataCollectionLibrary, 3287553425);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataCollectionLibrary(Z_Construct_UClass_UDataCollectionLibrary, &UDataCollectionLibrary::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("UDataCollectionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataCollectionLibrary);
class UScriptStruct* FJsonArrayItem::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FJsonArrayItem, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("JsonArrayItem"), sizeof(FJsonArrayItem), Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FJsonArrayItem(FJsonArrayItem::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("JsonArrayItem"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("JsonArrayItem")),new UScriptStruct::TCppStructOps<FJsonArrayItem>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem;
	UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("JsonArrayItem"), sizeof(FJsonArrayItem), Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("JsonArrayItem"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FJsonArrayItem>, EStructFlags(0x00000001));
			UProperty* NewProp_EventPositionZ = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionZ, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventPositionY = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionY"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionY, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventPositionX = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionX"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionX, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventTime = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventTime"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventTime, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventName, FJsonArrayItem), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionZ, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionY, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionX, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventTime, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventName, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC() { return 533802810U; }
class UScriptStruct* FCustomEvent::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCustomEvent, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("CustomEvent"), sizeof(FCustomEvent), Get_Z_Construct_UScriptStruct_FCustomEvent_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCustomEvent(FCustomEvent::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("CustomEvent"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CustomEvent")),new UScriptStruct::TCppStructOps<FCustomEvent>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent;
	UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CustomEvent"), sizeof(FCustomEvent), Get_Z_Construct_UScriptStruct_FCustomEvent_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CustomEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FCustomEvent>, EStructFlags(0x00000001));
			UProperty* NewProp_eventPosition = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventPosition"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(eventPosition, FCustomEvent), 0x0010000000000000, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_timeOfEvent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("timeOfEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(timeOfEvent, FCustomEvent), 0x0010000000000000);
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, FCustomEvent), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventPosition, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_timeOfEvent, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventName, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC() { return 3374977484U; }
	void UMyGameInstance::StaticRegisterNativesUMyGameInstance()
	{
		UClass* Class = UMyGameInstance::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetEventLocations", (Native)&UMyGameInstance::execGetEventLocations },
			{ "GetListOfFilesAtTempLocation", (Native)&UMyGameInstance::execGetListOfFilesAtTempLocation },
			{ "GetLoadedEventNames", (Native)&UMyGameInstance::execGetLoadedEventNames },
			{ "PrintEventBufferToFileIfExecutable", (Native)&UMyGameInstance::execPrintEventBufferToFileIfExecutable },
			{ "ReadJsonFileToDisplayBuffer", (Native)&UMyGameInstance::execReadJsonFileToDisplayBuffer },
			{ "SetCurrentDisplayedEvent", (Native)&UMyGameInstance::execSetCurrentDisplayedEvent },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 6);
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetEventLocations()
	{
		struct MyGameInstance_eventGetEventLocations_Parms
		{
			TArray<FVector> ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetEventLocations"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventGetEventLocations_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ReturnValue, MyGameInstance_eventGetEventLocations_Parms), 0x0010000000000580);
			UProperty* NewProp_ReturnValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FVector());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("GetEventLocations"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("GetEventLocations"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Get Event Locations"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation()
	{
		struct MyGameInstance_eventGetListOfFilesAtTempLocation_Parms
		{
			TArray<FString> Files;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetListOfFilesAtTempLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04420401, 65535, sizeof(MyGameInstance_eventGetListOfFilesAtTempLocation_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), sizeof(bool), true);
			UProperty* NewProp_Files = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Files"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(Files, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), 0x0010000000000180);
			UProperty* NewProp_Files_Inner = new(EC_InternalUseOnlyConstructor, NewProp_Files, TEXT("Files"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("GetListOfFilesAtTempLocation"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("GetListOfFilesAtTempLocation"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Get List Of Files At Temp Location"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames()
	{
		struct MyGameInstance_eventGetLoadedEventNames_Parms
		{
			TArray<FString> ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetLoadedEventNames"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventGetLoadedEventNames_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ReturnValue, MyGameInstance_eventGetLoadedEventNames_Parms), 0x0010000000000580);
			UProperty* NewProp_ReturnValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("GetLoadedEventNames"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("GetLoadedEventNames"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Get Loaded Event Names"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_PrintEventBufferToFileIfExecutable()
	{
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrintEventBufferToFileIfExecutable"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("PrintEventBufferToFileIfExecutable"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("PrintEventBufferToFileIfExecutable"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Print Event Buffer To File"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_ReadJsonFileToDisplayBuffer()
	{
		struct MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms
		{
			FString filename;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ReadJsonFileToDisplayBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms), sizeof(bool), true);
			UProperty* NewProp_filename = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("filename"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(filename, MyGameInstance_eventReadJsonFileToDisplayBuffer_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("ReadJsonFileToDisplayBuffer"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("ReadJsonFileToDisplayBuffer"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Read Json File To Display Buffer"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_filename, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent()
	{
		struct MyGameInstance_eventSetCurrentDisplayedEvent_Parms
		{
			FString eventName;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetCurrentDisplayedEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventSetCurrentDisplayedEvent_Parms));
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, MyGameInstance_eventSetCurrentDisplayedEvent_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("CompactNodeTitle"), TEXT("SetCurrentDisplayedEvent"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("SetCurrentDisplayedEvent"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Set Current Displayed Event"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMyGameInstance_NoRegister()
	{
		return UMyGameInstance::StaticClass();
	}
	UClass* Z_Construct_UClass_UMyGameInstance()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UGameInstance();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = UMyGameInstance::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000088;

				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetEventLocations());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_PrintEventBufferToFileIfExecutable());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_ReadJsonFileToDisplayBuffer());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_m_eventBuffer = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("m_eventBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(m_eventBuffer, UMyGameInstance), 0x0040000000000000);
				UProperty* NewProp_m_eventBuffer_Inner = new(EC_InternalUseOnlyConstructor, NewProp_m_eventBuffer, TEXT("m_eventBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FCustomEvent());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetEventLocations(), "GetEventLocations"); // 3101438290
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation(), "GetListOfFilesAtTempLocation"); // 2053587519
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames(), "GetLoadedEventNames"); // 3025775908
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_PrintEventBufferToFileIfExecutable(), "PrintEventBufferToFileIfExecutable"); // 502469526
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_ReadJsonFileToDisplayBuffer(), "ReadJsonFileToDisplayBuffer"); // 231183052
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent(), "SetCurrentDisplayedEvent"); // 3579888260
				static TCppClassTypeInfo<TCppClassTypeTraits<UMyGameInstance> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("MyGameInstance.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
				MetaData->SetValue(NewProp_m_eventBuffer, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMyGameInstance, 2134190129);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMyGameInstance(Z_Construct_UClass_UMyGameInstance, &UMyGameInstance::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("UMyGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyGameInstance);
	UPackage* Z_Construct_UPackage__Script_PrometheusPlugin()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/PrometheusPlugin")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0x5275C51F;
			Guid.B = 0xB7B79C8B;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
