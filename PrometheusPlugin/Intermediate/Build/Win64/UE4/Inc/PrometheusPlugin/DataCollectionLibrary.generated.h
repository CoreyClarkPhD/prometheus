// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMyGameInstance;
struct FVector;
#ifdef PROMETHEUSPLUGIN_DataCollectionLibrary_generated_h
#error "DataCollectionLibrary.generated.h already included, missing '#pragma once' in DataCollectionLibrary.h"
#endif
#define PROMETHEUSPLUGIN_DataCollectionLibrary_generated_h

#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetGameInstancePointer) \
	{ \
		P_GET_OBJECT(UMyGameInstance,Z_Param_gameInstance); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UDataCollectionLibrary::SetGameInstancePointer(Z_Param_gameInstance); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCollectPositionalEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventType); \
		P_GET_STRUCT(FVector,Z_Param_position); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UDataCollectionLibrary::CollectPositionalEvent(Z_Param_eventType,Z_Param_position); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetGameInstancePointer) \
	{ \
		P_GET_OBJECT(UMyGameInstance,Z_Param_gameInstance); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UDataCollectionLibrary::SetGameInstancePointer(Z_Param_gameInstance); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCollectPositionalEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventType); \
		P_GET_STRUCT(FVector,Z_Param_position); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UDataCollectionLibrary::CollectPositionalEvent(Z_Param_eventType,Z_Param_position); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataCollectionLibrary(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary(); \
public: \
	DECLARE_CLASS(UDataCollectionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UDataCollectionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDataCollectionLibrary(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary(); \
public: \
	DECLARE_CLASS(UDataCollectionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UDataCollectionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataCollectionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataCollectionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataCollectionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataCollectionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataCollectionLibrary(UDataCollectionLibrary&&); \
	NO_API UDataCollectionLibrary(const UDataCollectionLibrary&); \
public:


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataCollectionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataCollectionLibrary(UDataCollectionLibrary&&); \
	NO_API UDataCollectionLibrary(const UDataCollectionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataCollectionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataCollectionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataCollectionLibrary)


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_12_PROLOG
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_RPC_WRAPPERS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_INCLASS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_INCLASS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DataCollectionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_DataCollectionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
