// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PrometheusPlugin.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1PrometheusPlugin() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API class UClass* Z_Construct_UClass_UGameInstance();
	ENGINE_API class UClass* Z_Construct_UClass_ACameraActor();

	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_CollectEvent();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_GetTimeAsUTC();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_GetYearsFromNow();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_LogData();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UDataCollectionLibrary();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_AHttpService_MyHttpCall();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_AHttpService_RequestEventData();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_AHttpService_RequestEventNames();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_AHttpService_SendEventData();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_AHttpService_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_AHttpService();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSuperNewCustomEvent();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FNewCustomEvent();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSmallEvent();
	PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GenerateJsonStringFromCollectedData();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetEventLocations();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_NewReadJsonFileToDisplayBuffer();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_PrintFileAnytime();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_PrintFileExecutable();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeDataPost();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeEventDataRequest();
	PROMETHEUSPLUGIN_API class UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeEventNameRequest();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_APrometheusCameraBase_NoRegister();
	PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_APrometheusCameraBase();
	PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	void UDataCollectionLibrary::StaticRegisterNativesUDataCollectionLibrary()
	{
		UClass* Class = UDataCollectionLibrary::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "CollectEvent", (Native)&UDataCollectionLibrary::execCollectEvent },
			{ "GetTimeAsUTC", (Native)&UDataCollectionLibrary::execGetTimeAsUTC },
			{ "GetYearsFromNow", (Native)&UDataCollectionLibrary::execGetYearsFromNow },
			{ "LogData", (Native)&UDataCollectionLibrary::execLogData },
			{ "SetGameInstancePointer", (Native)&UDataCollectionLibrary::execSetGameInstancePointer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 5);
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_CollectEvent()
	{
		struct DataCollectionLibrary_eventCollectEvent_Parms
		{
			FString eventType;
			TArray<FString> eventPayloadKeys;
			TArray<FString> eventPayloadValues;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CollectEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(DataCollectionLibrary_eventCollectEvent_Parms));
			UProperty* NewProp_eventPayloadValues = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventPayloadValues"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(eventPayloadValues, DataCollectionLibrary_eventCollectEvent_Parms), 0x0010000000000080);
			UProperty* NewProp_eventPayloadValues_Inner = new(EC_InternalUseOnlyConstructor, NewProp_eventPayloadValues, TEXT("eventPayloadValues"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_eventPayloadKeys = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventPayloadKeys"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(eventPayloadKeys, DataCollectionLibrary_eventCollectEvent_Parms), 0x0010000000000080);
			UProperty* NewProp_eventPayloadKeys_Inner = new(EC_InternalUseOnlyConstructor, NewProp_eventPayloadKeys, TEXT("eventPayloadKeys"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_eventType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventType"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventType, DataCollectionLibrary_eventCollectEvent_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Blueprint Functions"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_GetTimeAsUTC()
	{
		struct DataCollectionLibrary_eventGetTimeAsUTC_Parms
		{
			FDateTime dateTime;
			FString ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetTimeAsUTC"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04822401, 65535, sizeof(DataCollectionLibrary_eventGetTimeAsUTC_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(ReturnValue, DataCollectionLibrary_eventGetTimeAsUTC_Parms), 0x0010000000000580);
			UProperty* NewProp_dateTime = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("dateTime"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(dateTime, DataCollectionLibrary_eventGetTimeAsUTC_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FDateTime());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_GetYearsFromNow()
	{
		struct DataCollectionLibrary_eventGetYearsFromNow_Parms
		{
			TArray<int32> ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetYearsFromNow"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(DataCollectionLibrary_eventGetYearsFromNow_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ReturnValue, DataCollectionLibrary_eventGetYearsFromNow_Parms), 0x0010000000000580);
			UProperty* NewProp_ReturnValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_LogData()
	{
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("LogData"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer()
	{
		struct DataCollectionLibrary_eventSetGameInstancePointer_Parms
		{
			UMyGameInstance* gameInstance;
		};
		UObject* Outer=Z_Construct_UClass_UDataCollectionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetGameInstancePointer"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(DataCollectionLibrary_eventSetGameInstancePointer_Parms));
			UProperty* NewProp_gameInstance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("gameInstance"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(gameInstance, DataCollectionLibrary_eventSetGameInstancePointer_Parms), 0x0010000000000080, Z_Construct_UClass_UMyGameInstance_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataCollectionLibrary_NoRegister()
	{
		return UDataCollectionLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_UDataCollectionLibrary()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintFunctionLibrary();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = UDataCollectionLibrary::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000080;

				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_CollectEvent());
				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_GetTimeAsUTC());
				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_GetYearsFromNow());
				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_LogData());
				OuterClass->LinkChild(Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_CollectEvent(), "CollectEvent"); // 81631329
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_GetTimeAsUTC(), "GetTimeAsUTC"); // 915674850
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_GetYearsFromNow(), "GetYearsFromNow"); // 2158038849
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_LogData(), "LogData"); // 720536883
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UDataCollectionLibrary_SetGameInstancePointer(), "SetGameInstancePointer"); // 2501695046
				static TCppClassTypeInfo<TCppClassTypeTraits<UDataCollectionLibrary> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("DataCollectionLibrary.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/DataCollectionLibrary.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataCollectionLibrary, 2999022403);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataCollectionLibrary(Z_Construct_UClass_UDataCollectionLibrary, &UDataCollectionLibrary::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("UDataCollectionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataCollectionLibrary);
	void AHttpService::StaticRegisterNativesAHttpService()
	{
		UClass* Class = AHttpService::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "MyHttpCall", (Native)&AHttpService::execMyHttpCall },
			{ "RequestEventData", (Native)&AHttpService::execRequestEventData },
			{ "RequestEventNames", (Native)&AHttpService::execRequestEventNames },
			{ "SendEventData", (Native)&AHttpService::execSendEventData },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 4);
	}
	UFunction* Z_Construct_UFunction_AHttpService_MyHttpCall()
	{
		UObject* Outer=Z_Construct_UClass_AHttpService();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MyHttpCall"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/HttpService.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("The actual HTTP call"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AHttpService_RequestEventData()
	{
		struct HttpService_eventRequestEventData_Parms
		{
			FString dataToSend;
		};
		UObject* Outer=Z_Construct_UClass_AHttpService();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RequestEventData"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(HttpService_eventRequestEventData_Parms));
			UProperty* NewProp_dataToSend = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("dataToSend"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(dataToSend, HttpService_eventRequestEventData_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/HttpService.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AHttpService_RequestEventNames()
	{
		struct HttpService_eventRequestEventNames_Parms
		{
			FString dataToSend;
		};
		UObject* Outer=Z_Construct_UClass_AHttpService();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RequestEventNames"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(HttpService_eventRequestEventNames_Parms));
			UProperty* NewProp_dataToSend = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("dataToSend"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(dataToSend, HttpService_eventRequestEventNames_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/HttpService.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AHttpService_SendEventData()
	{
		struct HttpService_eventSendEventData_Parms
		{
			FString dataToSend;
		};
		UObject* Outer=Z_Construct_UClass_AHttpService();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SendEventData"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(HttpService_eventSendEventData_Parms));
			UProperty* NewProp_dataToSend = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("dataToSend"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(dataToSend, HttpService_eventSendEventData_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/HttpService.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AHttpService_NoRegister()
	{
		return AHttpService::StaticClass();
	}
	UClass* Z_Construct_UClass_AHttpService()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = AHttpService::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;

				OuterClass->LinkChild(Z_Construct_UFunction_AHttpService_MyHttpCall());
				OuterClass->LinkChild(Z_Construct_UFunction_AHttpService_RequestEventData());
				OuterClass->LinkChild(Z_Construct_UFunction_AHttpService_RequestEventNames());
				OuterClass->LinkChild(Z_Construct_UFunction_AHttpService_SendEventData());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AHttpService_MyHttpCall(), "MyHttpCall"); // 244493460
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AHttpService_RequestEventData(), "RequestEventData"); // 1869500050
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AHttpService_RequestEventNames(), "RequestEventNames"); // 168037510
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AHttpService_SendEventData(), "SendEventData"); // 2590447656
				static TCppClassTypeInfo<TCppClassTypeTraits<AHttpService> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("HttpService.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/HttpService.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHttpService, 3469421020);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHttpService(Z_Construct_UClass_AHttpService, &AHttpService::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("AHttpService"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHttpService);
class UScriptStruct* FJsonArrayItem::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FJsonArrayItem, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("JsonArrayItem"), sizeof(FJsonArrayItem), Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FJsonArrayItem(FJsonArrayItem::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("JsonArrayItem"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("JsonArrayItem")),new UScriptStruct::TCppStructOps<FJsonArrayItem>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFJsonArrayItem;
	UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("JsonArrayItem"), sizeof(FJsonArrayItem), Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("JsonArrayItem"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FJsonArrayItem>, EStructFlags(0x00000001));
			UProperty* NewProp_EventPositionZ = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionZ, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventPositionY = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionY"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionY, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventPositionX = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventPositionX"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventPositionX, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventTime = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventTime"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventTime, FJsonArrayItem), 0x0010000000000000);
			UProperty* NewProp_EventName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventName, FJsonArrayItem), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionZ, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionY, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventPositionX, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventTime, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventName, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FJsonArrayItem_CRC() { return 533802810U; }
class UScriptStruct* FSuperNewCustomEvent::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSuperNewCustomEvent();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FSuperNewCustomEvent_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSuperNewCustomEvent, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("SuperNewCustomEvent"), sizeof(FSuperNewCustomEvent), Get_Z_Construct_UScriptStruct_FSuperNewCustomEvent_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSuperNewCustomEvent(FSuperNewCustomEvent::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("SuperNewCustomEvent"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFSuperNewCustomEvent
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFSuperNewCustomEvent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("SuperNewCustomEvent")),new UScriptStruct::TCppStructOps<FSuperNewCustomEvent>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFSuperNewCustomEvent;
	UScriptStruct* Z_Construct_UScriptStruct_FSuperNewCustomEvent()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FSuperNewCustomEvent_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SuperNewCustomEvent"), sizeof(FSuperNewCustomEvent), Get_Z_Construct_UScriptStruct_FSuperNewCustomEvent_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SuperNewCustomEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FSuperNewCustomEvent>, EStructFlags(0x00000001));
			UProperty* NewProp_m_eventValues = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("m_eventValues"), RF_Public|RF_Transient|RF_MarkAsNative) UMapProperty(CPP_PROPERTY_BASE(m_eventValues, FSuperNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_m_eventValues_Key_KeyProp = new(EC_InternalUseOnlyConstructor, NewProp_m_eventValues, TEXT("m_eventValues_Key"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_m_eventValues_ValueProp = new(EC_InternalUseOnlyConstructor, NewProp_m_eventValues, TEXT("m_eventValues"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 1, 0x0000000000000000);
			UProperty* NewProp_EventDate = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventDate"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventDate, FSuperNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_EventType = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EventType"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(EventType, FSuperNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_ApplicationId = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("ApplicationId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(ApplicationId, FSuperNewCustomEvent), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_m_eventValues, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventDate, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_EventType, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_ApplicationId, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSuperNewCustomEvent_CRC() { return 1294043655U; }
class UScriptStruct* FNewCustomEvent::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FNewCustomEvent();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FNewCustomEvent_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNewCustomEvent, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("NewCustomEvent"), sizeof(FNewCustomEvent), Get_Z_Construct_UScriptStruct_FNewCustomEvent_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNewCustomEvent(FNewCustomEvent::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("NewCustomEvent"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFNewCustomEvent
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFNewCustomEvent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NewCustomEvent")),new UScriptStruct::TCppStructOps<FNewCustomEvent>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFNewCustomEvent;
	UScriptStruct* Z_Construct_UScriptStruct_FNewCustomEvent()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FNewCustomEvent_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NewCustomEvent"), sizeof(FNewCustomEvent), Get_Z_Construct_UScriptStruct_FNewCustomEvent_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("NewCustomEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FNewCustomEvent>, EStructFlags(0x00000001));
			UProperty* NewProp_m_eventValues = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("m_eventValues"), RF_Public|RF_Transient|RF_MarkAsNative) UMapProperty(CPP_PROPERTY_BASE(m_eventValues, FNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_m_eventValues_Key_KeyProp = new(EC_InternalUseOnlyConstructor, NewProp_m_eventValues, TEXT("m_eventValues_Key"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_m_eventValues_ValueProp = new(EC_InternalUseOnlyConstructor, NewProp_m_eventValues, TEXT("m_eventValues"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 1, 0x0000000000000000);
			UProperty* NewProp_m_eventTime = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("m_eventTime"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(m_eventTime, FNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_m_eventType = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("m_eventType"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(m_eventType, FNewCustomEvent), 0x0010000000000000);
			UProperty* NewProp_m_applicationId = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("m_applicationId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(m_applicationId, FNewCustomEvent), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_m_eventValues, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_m_eventTime, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_m_eventType, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_m_applicationId, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNewCustomEvent_CRC() { return 2434132244U; }
class UScriptStruct* FSmallEvent::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSmallEvent();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FSmallEvent_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSmallEvent, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("SmallEvent"), sizeof(FSmallEvent), Get_Z_Construct_UScriptStruct_FSmallEvent_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSmallEvent(FSmallEvent::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("SmallEvent"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFSmallEvent
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFSmallEvent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("SmallEvent")),new UScriptStruct::TCppStructOps<FSmallEvent>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFSmallEvent;
	UScriptStruct* Z_Construct_UScriptStruct_FSmallEvent()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FSmallEvent_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SmallEvent"), sizeof(FSmallEvent), Get_Z_Construct_UScriptStruct_FSmallEvent_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SmallEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FSmallEvent>, EStructFlags(0x00000001));
			UProperty* NewProp_eventPosition = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventPosition"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(eventPosition, FSmallEvent), 0x0010000000000000, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, FSmallEvent), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventPosition, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventName, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSmallEvent_CRC() { return 1812083815U; }
class UScriptStruct* FCustomEvent::StaticStruct()
{
	extern PROMETHEUSPLUGIN_API class UPackage* Z_Construct_UPackage__Script_PrometheusPlugin();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent();
		extern PROMETHEUSPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCustomEvent, Z_Construct_UPackage__Script_PrometheusPlugin(), TEXT("CustomEvent"), sizeof(FCustomEvent), Get_Z_Construct_UScriptStruct_FCustomEvent_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCustomEvent(FCustomEvent::StaticStruct, TEXT("/Script/PrometheusPlugin"), TEXT("CustomEvent"), false, nullptr, nullptr);
static struct FScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent
{
	FScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CustomEvent")),new UScriptStruct::TCppStructOps<FCustomEvent>);
	}
} ScriptStruct_PrometheusPlugin_StaticRegisterNativesFCustomEvent;
	UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_PrometheusPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CustomEvent"), sizeof(FCustomEvent), Get_Z_Construct_UScriptStruct_FCustomEvent_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CustomEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FCustomEvent>, EStructFlags(0x00000001));
			UProperty* NewProp_eventPosition = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventPosition"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(eventPosition, FCustomEvent), 0x0010000000000000, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_timeOfEvent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("timeOfEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(timeOfEvent, FCustomEvent), 0x0010000000000000);
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, FCustomEvent), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventPosition, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_timeOfEvent, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_eventName, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCustomEvent_CRC() { return 3374977484U; }
	void UMyGameInstance::StaticRegisterNativesUMyGameInstance()
	{
		UClass* Class = UMyGameInstance::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GenerateJsonStringFromCollectedData", (Native)&UMyGameInstance::execGenerateJsonStringFromCollectedData },
			{ "GetEventLocations", (Native)&UMyGameInstance::execGetEventLocations },
			{ "GetListOfFilesAtTempLocation", (Native)&UMyGameInstance::execGetListOfFilesAtTempLocation },
			{ "GetLoadedEventNames", (Native)&UMyGameInstance::execGetLoadedEventNames },
			{ "NewReadJsonFileToDisplayBuffer", (Native)&UMyGameInstance::execNewReadJsonFileToDisplayBuffer },
			{ "PrintFileAnytime", (Native)&UMyGameInstance::execPrintFileAnytime },
			{ "PrintFileExecutable", (Native)&UMyGameInstance::execPrintFileExecutable },
			{ "SetCurrentDisplayedEvent", (Native)&UMyGameInstance::execSetCurrentDisplayedEvent },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 8);
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GenerateJsonStringFromCollectedData()
	{
		struct MyGameInstance_eventGenerateJsonStringFromCollectedData_Parms
		{
			FString ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GenerateJsonStringFromCollectedData"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventGenerateJsonStringFromCollectedData_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(ReturnValue, MyGameInstance_eventGenerateJsonStringFromCollectedData_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetEventLocations()
	{
		struct MyGameInstance_eventGetEventLocations_Parms
		{
			TArray<FVector> ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetEventLocations"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventGetEventLocations_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ReturnValue, MyGameInstance_eventGetEventLocations_Parms), 0x0010000000000580);
			UProperty* NewProp_ReturnValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FVector());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation()
	{
		struct MyGameInstance_eventGetListOfFilesAtTempLocation_Parms
		{
			TArray<FString> Files;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetListOfFilesAtTempLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04420401, 65535, sizeof(MyGameInstance_eventGetListOfFilesAtTempLocation_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), sizeof(bool), true);
			UProperty* NewProp_Files = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Files"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(Files, MyGameInstance_eventGetListOfFilesAtTempLocation_Parms), 0x0010000000000180);
			UProperty* NewProp_Files_Inner = new(EC_InternalUseOnlyConstructor, NewProp_Files, TEXT("Files"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames()
	{
		struct MyGameInstance_eventGetLoadedEventNames_Parms
		{
			TArray<FString> ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetLoadedEventNames"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventGetLoadedEventNames_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ReturnValue, MyGameInstance_eventGetLoadedEventNames_Parms), 0x0010000000000580);
			UProperty* NewProp_ReturnValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_NewReadJsonFileToDisplayBuffer()
	{
		struct MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms
		{
			FString filename;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("NewReadJsonFileToDisplayBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms), sizeof(bool), true);
			UProperty* NewProp_filename = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("filename"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(filename, MyGameInstance_eventNewReadJsonFileToDisplayBuffer_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
			MetaData->SetValue(NewProp_filename, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_PrintFileAnytime()
	{
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrintFileAnytime"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_PrintFileExecutable()
	{
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrintFileExecutable"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent()
	{
		struct MyGameInstance_eventSetCurrentDisplayedEvent_Parms
		{
			FString eventName;
		};
		UObject* Outer=Z_Construct_UClass_UMyGameInstance();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetCurrentDisplayedEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(MyGameInstance_eventSetCurrentDisplayedEvent_Parms));
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, MyGameInstance_eventSetCurrentDisplayedEvent_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMyGameInstance_NoRegister()
	{
		return UMyGameInstance::StaticClass();
	}
	UClass* Z_Construct_UClass_UMyGameInstance()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UGameInstance();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = UMyGameInstance::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000088;

				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GenerateJsonStringFromCollectedData());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetEventLocations());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_NewReadJsonFileToDisplayBuffer());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_PrintFileAnytime());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_PrintFileExecutable());
				OuterClass->LinkChild(Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_m_eventBuffer = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("m_eventBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(m_eventBuffer, UMyGameInstance), 0x0010000000000000);
				UProperty* NewProp_m_eventBuffer_Inner = new(EC_InternalUseOnlyConstructor, NewProp_m_eventBuffer, TEXT("m_eventBuffer"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FCustomEvent());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GenerateJsonStringFromCollectedData(), "GenerateJsonStringFromCollectedData"); // 2787630342
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetEventLocations(), "GetEventLocations"); // 274792375
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetListOfFilesAtTempLocation(), "GetListOfFilesAtTempLocation"); // 401397226
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_GetLoadedEventNames(), "GetLoadedEventNames"); // 724617788
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_NewReadJsonFileToDisplayBuffer(), "NewReadJsonFileToDisplayBuffer"); // 365229622
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_PrintFileAnytime(), "PrintFileAnytime"); // 1065017250
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_PrintFileExecutable(), "PrintFileExecutable"); // 3669481643
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UMyGameInstance_SetCurrentDisplayedEvent(), "SetCurrentDisplayedEvent"); // 1664857446
				static TCppClassTypeInfo<TCppClassTypeTraits<UMyGameInstance> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("MyGameInstance.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
				MetaData->SetValue(NewProp_m_eventBuffer, TEXT("ModuleRelativePath"), TEXT("Public/MyGameInstance.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMyGameInstance, 1638496296);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMyGameInstance(Z_Construct_UClass_UMyGameInstance, &UMyGameInstance::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("UMyGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyGameInstance);
	void APrometheusCameraBase::StaticRegisterNativesAPrometheusCameraBase()
	{
		UClass* Class = APrometheusCameraBase::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "MakeDataPost", (Native)&APrometheusCameraBase::execMakeDataPost },
			{ "MakeEventDataRequest", (Native)&APrometheusCameraBase::execMakeEventDataRequest },
			{ "MakeEventNameRequest", (Native)&APrometheusCameraBase::execMakeEventNameRequest },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 3);
	}
	UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeDataPost()
	{
		struct PrometheusCameraBase_eventMakeDataPost_Parms
		{
			FString jsonString;
		};
		UObject* Outer=Z_Construct_UClass_APrometheusCameraBase();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MakeDataPost"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(PrometheusCameraBase_eventMakeDataPost_Parms));
			UProperty* NewProp_jsonString = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("jsonString"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(jsonString, PrometheusCameraBase_eventMakeDataPost_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeEventDataRequest()
	{
		struct PrometheusCameraBase_eventMakeEventDataRequest_Parms
		{
			FString eventName;
			FString startDate;
			FString endDate;
		};
		UObject* Outer=Z_Construct_UClass_APrometheusCameraBase();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MakeEventDataRequest"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(PrometheusCameraBase_eventMakeEventDataRequest_Parms));
			UProperty* NewProp_endDate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("endDate"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(endDate, PrometheusCameraBase_eventMakeEventDataRequest_Parms), 0x0010000000000080);
			UProperty* NewProp_startDate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("startDate"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(startDate, PrometheusCameraBase_eventMakeEventDataRequest_Parms), 0x0010000000000080);
			UProperty* NewProp_eventName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("eventName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(eventName, PrometheusCameraBase_eventMakeEventDataRequest_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APrometheusCameraBase_MakeEventNameRequest()
	{
		UObject* Outer=Z_Construct_UClass_APrometheusCameraBase();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MakeEventNameRequest"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Prometheus"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Blueprint available functions for calling the http functions"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APrometheusCameraBase_NoRegister()
	{
		return APrometheusCameraBase::StaticClass();
	}
	UClass* Z_Construct_UClass_APrometheusCameraBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACameraActor();
			Z_Construct_UPackage__Script_PrometheusPlugin();
			OuterClass = APrometheusCameraBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_APrometheusCameraBase_MakeDataPost());
				OuterClass->LinkChild(Z_Construct_UFunction_APrometheusCameraBase_MakeEventDataRequest());
				OuterClass->LinkChild(Z_Construct_UFunction_APrometheusCameraBase_MakeEventNameRequest());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(WasLastCallData, APrometheusCameraBase, bool);
				UProperty* NewProp_WasLastCallData = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WasLastCallData"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(WasLastCallData, APrometheusCameraBase), 0x0010000000000004, CPP_BOOL_PROPERTY_BITMASK(WasLastCallData, APrometheusCameraBase), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(WasRequestSuccessful, APrometheusCameraBase, bool);
				UProperty* NewProp_WasRequestSuccessful = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WasRequestSuccessful"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(WasRequestSuccessful, APrometheusCameraBase), 0x0010000000000004, CPP_BOOL_PROPERTY_BITMASK(WasRequestSuccessful, APrometheusCameraBase), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(DoneWaiting, APrometheusCameraBase, bool);
				UProperty* NewProp_DoneWaiting = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DoneWaiting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(DoneWaiting, APrometheusCameraBase), 0x0010000000000004, CPP_BOOL_PROPERTY_BITMASK(DoneWaiting, APrometheusCameraBase), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_APrometheusCameraBase_MakeDataPost(), "MakeDataPost"); // 2460492601
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_APrometheusCameraBase_MakeEventDataRequest(), "MakeEventDataRequest"); // 2003393185
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_APrometheusCameraBase_MakeEventNameRequest(), "MakeEventNameRequest"); // 2503293988
				static TCppClassTypeInfo<TCppClassTypeTraits<APrometheusCameraBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Rendering"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PrometheusCameraBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_WasLastCallData, TEXT("Category"), TEXT("PrometheusCameraBase"));
				MetaData->SetValue(NewProp_WasLastCallData, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
				MetaData->SetValue(NewProp_WasRequestSuccessful, TEXT("Category"), TEXT("PrometheusCameraBase"));
				MetaData->SetValue(NewProp_WasRequestSuccessful, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
				MetaData->SetValue(NewProp_DoneWaiting, TEXT("Category"), TEXT("PrometheusCameraBase"));
				MetaData->SetValue(NewProp_DoneWaiting, TEXT("ModuleRelativePath"), TEXT("Public/PrometheusCameraBase.h"));
				MetaData->SetValue(NewProp_DoneWaiting, TEXT("ToolTip"), TEXT("Variables for passing information up to the blueprint code (for waiting screen)"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(APrometheusCameraBase, 2017276274);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APrometheusCameraBase(Z_Construct_UClass_APrometheusCameraBase, &APrometheusCameraBase::StaticClass, TEXT("/Script/PrometheusPlugin"), TEXT("APrometheusCameraBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APrometheusCameraBase);
	UPackage* Z_Construct_UPackage__Script_PrometheusPlugin()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/PrometheusPlugin")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0x381ABE42;
			Guid.B = 0x8C2BAD0D;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
