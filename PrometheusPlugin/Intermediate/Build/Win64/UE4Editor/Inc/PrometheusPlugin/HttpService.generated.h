// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROMETHEUSPLUGIN_HttpService_generated_h
#error "HttpService.generated.h already included, missing '#pragma once' in HttpService.h"
#endif
#define PROMETHEUSPLUGIN_HttpService_generated_h

#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSendEventData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SendEventData(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRequestEventData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RequestEventData(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRequestEventNames) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RequestEventNames(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMyHttpCall) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MyHttpCall(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSendEventData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SendEventData(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRequestEventData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RequestEventData(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRequestEventNames) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_dataToSend); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->RequestEventNames(Z_Param_dataToSend); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMyHttpCall) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MyHttpCall(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHttpService(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_AHttpService(); \
public: \
	DECLARE_CLASS(AHttpService, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(AHttpService) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAHttpService(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_AHttpService(); \
public: \
	DECLARE_CLASS(AHttpService, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(AHttpService) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHttpService(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHttpService) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHttpService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHttpService); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHttpService(AHttpService&&); \
	NO_API AHttpService(const AHttpService&); \
public:


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHttpService(AHttpService&&); \
	NO_API AHttpService(const AHttpService&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHttpService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHttpService); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHttpService)


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_PRIVATE_PROPERTY_OFFSET
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_14_PROLOG
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_RPC_WRAPPERS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_INCLASS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_INCLASS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_HttpService_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
