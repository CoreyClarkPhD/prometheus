// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROMETHEUSPLUGIN_PrometheusCameraBase_generated_h
#error "PrometheusCameraBase.generated.h already included, missing '#pragma once' in PrometheusCameraBase.h"
#endif
#define PROMETHEUSPLUGIN_PrometheusCameraBase_generated_h

#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMakeDataPost) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_jsonString); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeDataPost(Z_Param_jsonString); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeEventDataRequest) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_startDate); \
		P_GET_PROPERTY(UStrProperty,Z_Param_endDate); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeEventDataRequest(Z_Param_eventName,Z_Param_startDate,Z_Param_endDate); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeEventNameRequest) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeEventNameRequest(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMakeDataPost) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_jsonString); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeDataPost(Z_Param_jsonString); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeEventDataRequest) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_startDate); \
		P_GET_PROPERTY(UStrProperty,Z_Param_endDate); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeEventDataRequest(Z_Param_eventName,Z_Param_startDate,Z_Param_endDate); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeEventNameRequest) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->MakeEventNameRequest(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPrometheusCameraBase(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_APrometheusCameraBase(); \
public: \
	DECLARE_CLASS(APrometheusCameraBase, ACameraActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(APrometheusCameraBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPrometheusCameraBase(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_APrometheusCameraBase(); \
public: \
	DECLARE_CLASS(APrometheusCameraBase, ACameraActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(APrometheusCameraBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APrometheusCameraBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APrometheusCameraBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APrometheusCameraBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APrometheusCameraBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APrometheusCameraBase(APrometheusCameraBase&&); \
	NO_API APrometheusCameraBase(const APrometheusCameraBase&); \
public:


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APrometheusCameraBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APrometheusCameraBase(APrometheusCameraBase&&); \
	NO_API APrometheusCameraBase(const APrometheusCameraBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APrometheusCameraBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APrometheusCameraBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APrometheusCameraBase)


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_PRIVATE_PROPERTY_OFFSET
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_12_PROLOG
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_RPC_WRAPPERS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_INCLASS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_INCLASS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_PrometheusCameraBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
