// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef PROMETHEUSPLUGIN_MyGameInstance_generated_h
#error "MyGameInstance.generated.h already included, missing '#pragma once' in MyGameInstance.h"
#endif
#define PROMETHEUSPLUGIN_MyGameInstance_generated_h

#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_78_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FJsonArrayItem(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_59_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSuperNewCustomEvent(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_41_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FNewCustomEvent(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_29_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FSmallEvent(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_14_GENERATED_BODY \
	friend PROMETHEUSPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FCustomEvent(); \
	PROMETHEUSPLUGIN_API static class UScriptStruct* StaticStruct();


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGenerateJsonStringFromCollectedData) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->GenerateJsonStringFromCollectedData(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execNewReadJsonFileToDisplayBuffer) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->NewReadJsonFileToDisplayBuffer(Z_Param_filename); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetListOfFilesAtTempLocation) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Files); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetListOfFilesAtTempLocation(Z_Param_Out_Files); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFileAnytime) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintFileAnytime(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFileExecutable) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintFileExecutable(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEventLocations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FVector>*)Z_Param__Result=this->GetEventLocations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentDisplayedEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetCurrentDisplayedEvent(Z_Param_eventName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoadedEventNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=this->GetLoadedEventNames(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGenerateJsonStringFromCollectedData) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->GenerateJsonStringFromCollectedData(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execNewReadJsonFileToDisplayBuffer) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->NewReadJsonFileToDisplayBuffer(Z_Param_filename); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetListOfFilesAtTempLocation) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Files); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->GetListOfFilesAtTempLocation(Z_Param_Out_Files); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFileAnytime) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintFileAnytime(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFileExecutable) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PrintFileExecutable(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEventLocations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FVector>*)Z_Param__Result=this->GetEventLocations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetCurrentDisplayedEvent) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_eventName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetCurrentDisplayedEvent(Z_Param_eventName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLoadedEventNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=this->GetLoadedEventNames(); \
		P_NATIVE_END; \
	}


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance(); \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_INCLASS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend PROMETHEUSPLUGIN_API class UClass* Z_Construct_UClass_UMyGameInstance(); \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/PrometheusPlugin"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyGameInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public:


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMyGameInstance)


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_PRIVATE_PROPERTY_OFFSET
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_97_PROLOG
#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_RPC_WRAPPERS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_INCLASS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_PRIVATE_PROPERTY_OFFSET \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_RPC_WRAPPERS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_INCLASS_NO_PURE_DECLS \
	LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h_100_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LostInTheDark_16_Plugins_PrometheusPlugin_Source_PrometheusPlugin_Public_MyGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
