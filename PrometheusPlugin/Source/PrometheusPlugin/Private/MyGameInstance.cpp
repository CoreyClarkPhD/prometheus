// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameInstance.h"
#include "EngineGlobals.h"
#include "Engine.h"
#include "RenderCore.h"
#include "Timespan.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "PrometheusCameraBase.h"
#include "EngineUtils.h"

//-----------------------------------------------------------------------------------------------------

UMyGameInstance::UMyGameInstance()
{
}

//-----------------------------------------------------------------------------------------------------

UMyGameInstance::~UMyGameInstance()
{
}

//-----------------------------------------------------------------------------------------------------

TArray<FVector> UMyGameInstance::GetEventLocations()
{
	TArray<FVector> eventLocations;

	for (int i = 0; i < m_loadedFileBuffer.Num(); i++)
	{
		if (m_loadedFileBuffer[i].eventName == m_currentDisplayedEvent || m_currentDisplayedEvent == "All")//should store this array since it won't change until there's input from the user so we don't have to calc every frame
		{
			eventLocations.Emplace(m_loadedFileBuffer[i].eventPosition);
		}
	}

	return eventLocations;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::IsRunningInExecutable()
{
	if (GetWorld()->IsPlayInEditor())
	{
		return false;
	}
	else
	{
		return true;
	}
}

//-----------------------------------------------------------------------------------------------------

TArray<FString> UMyGameInstance::GetLoadedEventNames()
{
	return m_loadedEventNames;
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::SetCurrentDisplayedEvent(FString eventName)
{
	m_currentDisplayedEvent = eventName;
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::StoreEvent(FString eventType, TArray<FString> eventPayloadKeys, TArray<FString> eventPayloadValues)
{
	if (GetWorld() != nullptr)
	{
		//creating blank custom event
		FNewCustomEvent e;

		//Get application name, set to application id
		FString dirPath = FString(FPaths::GameDir());
		e.m_applicationId = FPaths::GetBaseFilename(dirPath);

		//get current time utc*
		e.m_eventTime = FDateTime::UtcNow().ToString();

		//Package into m_eventBuffer.Emplace
		e.m_eventType = eventType;
		
		//loop through both arrays and assign them as key:value pairs
		for (int i = 0; i < eventPayloadKeys.Num(); i++)
		{
			e.m_eventValues.Add(eventPayloadKeys[i], eventPayloadValues[i]);
		}

		m_newEventBuffer.Emplace(e);
	}
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::SaveStringToDataFile(
	const FString& SaveDirectory,
	const FString&  FileName,
	const FString&  TextToSave,
	bool AllowOverwriting
) {
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	// CreateDirectoryTree returns true if the destination
	// directory existed prior to call or has been created
	// during the call.

	if (PlatformFile.CreateDirectoryTree(*SaveDirectory))
	{

		// Get absolute file path
		FString AbsoluteFilePath = SaveDirectory + "/" + FileName;

		// Allow overwriting or file doesn't already exist
		if (AllowOverwriting || !PlatformFile.FileExists(*AbsoluteFilePath))
		{
			//Save the string to file
			bool didSave = FFileHelper::SaveStringToFile(TextToSave, *AbsoluteFilePath);
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::LoadDataFileToString(const FString& LoadDirectory, const FString& FileName, FString& StringToLoad)
{
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	// CreateDirectoryTree returns true if the destination
	// directory existed prior to call or has been created
	// during the call.
	if (PlatformFile.DirectoryExists(*LoadDirectory))
	{
		// Get absolute file path
		FString AbsoluteFilePath = LoadDirectory + "/" + FileName;

		// Allow overwriting or file doesn't already exist
		if (PlatformFile.FileExists(*AbsoluteFilePath))
		{
			bool didLoad = FFileHelper::LoadFileToString(StringToLoad, *AbsoluteFilePath);
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

FString UMyGameInstance::GetFilename()
{
	FString dirPath = FString(FPaths::GameDir());

	FDateTime currentTime = FDateTime::Now();
	FString year = FString::FromInt(currentTime.GetYear());
	FString month = FString::FromInt(currentTime.GetMonth());
	FString day = FString::FromInt(currentTime.GetDay());
	FString hour = FString::FromInt(currentTime.GetHour());
	FString minute = FString::FromInt(currentTime.GetMinute());
	FString second = FString::FromInt(currentTime.GetSecond());

	FString gameName = FPaths::GetBaseFilename(dirPath);

	FString fileDate = year + "-" + month + "-" + day;
	FString fileTime = hour + "-" + minute + "-" + second;

	FString fileName = gameName + "__" + fileDate + "__" + fileTime + "__PrometheusOutput.json";

	return fileName;
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::ClearEventBuffer()
{
	//Made into a function so it can be called from the Prometheus Camera Base
	m_eventBuffer.Empty();
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::PrintFileAnytime()
{
	//Get the filename for the text file
	FString fileName = GetFilename();

	//Get the text for the data file
	FString OutputString = GenerateJsonStringFromCollectedData();

	//Get the file directory
	FString dirPath = FString(FPaths::GameDir());
	FString saveDirectory = dirPath + "Temp/";

	//Save the file to the file directory
	bool didSave = SaveStringToDataFile(saveDirectory, fileName, OutputString, true);

	//Empty the event buffer
	ClearEventBuffer();
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::SaveDataToServerOrFile()
{
	//Need to find the Prometheus camera in the scene, as it handles all HTTP requests
	for (TActorIterator<APrometheusCameraBase> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		//If we have found the Prometheus Camera
		if (ActorItr->GetName().Contains("PrometheusCamera"))
		{
			//Get the collected data as a Json String
			FString jsonData = GenerateJsonStringFromCollectedData();

			//Make Data Post on the camera (event info is cleared by the camera upon completion
			ActorItr->MakeDataPost(jsonData);
		}
	}
}

//-----------------------------------------------------------------------------------------------------

void UMyGameInstance::PrintFileExecutable()
{
	if (IsRunningInExecutable())
	{
		PrintFileAnytime();
	}
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::GetListOfFilesAtTempLocation(TArray<FString>& Files) //credit to RAMA for this: https://answers.unrealengine.com/questions/61858/using-ffilemanagergeneric-for-listing-files-in-dir.html
{
	//if (RootFolderFullPath.Len() < 1) return false;

	//FPaths::NormalizeDirectoryName(RootFolderFullPath);

	IFileManager& FileManager = IFileManager::Get();

	FString Ext = ".json";

	if (Ext == "")
	{
		Ext = "*.*";
	}
	else
	{
		Ext = (Ext.Left(1) == ".") ? "*" + Ext : "*." + Ext;
	}

	FString dirPath = FString(FPaths::GameDir());
	FString fileDirectory = dirPath + "Temp";

	FString FinalPath = fileDirectory + "/" + Ext;
	FileManager.FindFiles(Files, *FinalPath, true, false);
	return true;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::ReadInJsonStringToDisplayAndEventBuffers(const FString& jsonString)
{
	TArray<FSuperNewCustomEvent> arrayOfEvents;

	TArray<TSharedPtr<FJsonValue> > JsonArray;
	TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(jsonString);
	if (!FJsonSerializer::Deserialize(JsonReader, JsonArray))
	{
		return false;
	}

	FJsonObjectConverter::JsonArrayStringToUStruct(jsonString, &arrayOfEvents, 0, 0);

	for (int32 i = 0; i < JsonArray.Num(); i++)
	{
		TSharedPtr<FJsonObject> Thing = JsonArray[i]->AsObject();
		TSharedPtr<FJsonObject> SubThing = Thing->GetObjectField("EventPayload");

		for (auto currObject = SubThing->Values.CreateConstIterator(); currObject; ++currObject)
		{

			FString key = currObject->Key;
			TSharedPtr<FJsonValue> jsonValue = currObject->Value;
			FString value = jsonValue->AsString();

			FVector positionalVector;
			positionalVector.InitFromString(value);

			arrayOfEvents[i].m_eventValues.Add(key, value);
		}

	}

	for (int i = 0; i < arrayOfEvents.Num(); i++)
	{
		for (auto& Elem : arrayOfEvents[i].m_eventValues)
		{
			if (Elem.Key == "WorldLocation")
			{
				FVector position;
				position.InitFromString(Elem.Value);

				FSmallEvent event;
				event.eventName = arrayOfEvents[i].EventType;
				event.eventPosition = position;


				bool didFind = false;
				for (int j = 0; j < m_loadedEventNames.Num(); j++)
				{
					if (event.eventName == m_loadedEventNames[j])
					{
						didFind = true;
					}
				}

				if (!didFind)
				{
					m_loadedEventNames.Emplace(event.eventName);
				}

				m_loadedFileBuffer.Emplace(event);
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::ReadJsonStringIntoEventBuffer(const TArray<FString>& eventArray)
{
	m_loadedEventNames.Empty();

	for (int i = 0; i < eventArray.Num(); i++)
	{
		bool didFind = false;
		for (int j = 0; j < m_loadedEventNames.Num(); j++)
		{
			if (eventArray[i] == m_loadedEventNames[j])
			{
				didFind = true;
			}
		}

		if (!didFind)
		{
			m_loadedEventNames.Emplace(eventArray[i]);
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::ReadJsonIntoOnlyDisplayBuffer(const FString& jsonString)
{
	TArray<FSuperNewCustomEvent> arrayOfEvents;

	TArray<TSharedPtr<FJsonValue> > JsonArray;
	TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(jsonString);
	if (!FJsonSerializer::Deserialize(JsonReader, JsonArray))
	{
		return false;
	}

	FJsonObjectConverter::JsonArrayStringToUStruct(jsonString, &arrayOfEvents, 0, 0);

	for (int32 i = 0; i < JsonArray.Num(); i++)
	{
		TSharedPtr<FJsonObject> Thing = JsonArray[i]->AsObject();
		TSharedPtr<FJsonObject> SubThing = Thing->GetObjectField("EventPayload");

		for (auto currObject = SubThing->Values.CreateConstIterator(); currObject; ++currObject)
		{

			FString key = currObject->Key;
			TSharedPtr<FJsonValue> jsonValue = currObject->Value;
			FString value = jsonValue->AsString();

			FVector positionalVector;
			positionalVector.InitFromString(value);

			arrayOfEvents[i].m_eventValues.Add(key, value);
		}

	}

	for (int i = 0; i < arrayOfEvents.Num(); i++)
	{
		for (auto& Elem : arrayOfEvents[i].m_eventValues)
		{
			if (Elem.Key == "WorldLocation")
			{
				FVector position;
				position.InitFromString(Elem.Value);

				FSmallEvent event;
				event.eventName = arrayOfEvents[i].EventType;
				event.eventPosition = position;

				m_loadedFileBuffer.Emplace(event);
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------

bool UMyGameInstance::NewReadJsonFileToDisplayBuffer(const FString& filename)
{
	m_loadedFileBuffer.Empty();
	m_loadedEventNames.Empty();

	FString dirPath = FString(FPaths::GameDir());
	FString loadDirectory = dirPath + "Temp/";

	FString FullJsonString;
	LoadDataFileToString(loadDirectory, filename, FullJsonString);

	ReadInJsonStringToDisplayAndEventBuffers(FullJsonString);

	return false;
}

//-----------------------------------------------------------------------------------------------------

FString UMyGameInstance::GenerateJsonStringFromCollectedData()
{
	// create Json values array
	TArray< TSharedPtr<FJsonValue> > AllJsonArray;

	for (int i = 0; i < m_newEventBuffer.Num(); i++)
	{
		//create a blank Json object:
		TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

		//Populate the Json object with the desired fields:
		JsonObject->SetStringField("ApplicationId", m_newEventBuffer[i].m_applicationId);
		JsonObject->SetStringField("EventType", m_newEventBuffer[i].m_eventType);
		JsonObject->SetStringField("EventDate", m_newEventBuffer[i].m_eventTime);

		TSharedPtr<FJsonObject> EventPayloadObject = MakeShareable(new FJsonObject);

		for (auto It = m_newEventBuffer[i].m_eventValues.CreateConstIterator(); It; ++It)
		{
			EventPayloadObject->SetStringField(It.Key(), It.Value());
		}

		JsonObject->SetObjectField("EventPayload", EventPayloadObject);

		//Add the Json object to the array:
		TSharedRef< FJsonValueObject > JsonValue = MakeShareable(new FJsonValueObject(JsonObject));
		AllJsonArray.Add(JsonValue);
	}

	//Serialize the Json Array into OutputString:
	FString OutputString;
	TSharedRef<TJsonWriter<>> writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(AllJsonArray, writer);

	return OutputString;
}

//-----------------------------------------------------------------------------------------------------