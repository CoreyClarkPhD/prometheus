// Fill out your copyright notice in the Description page of Project Settings.

#include "DataCollectionLibrary.h"
#include "PlatformFilemanager.h"
#include "FileHelper.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstance.h"
#include "EngineGlobals.h"
#include "Engine.h"
#include "MyGameInstance.h"

//-----------------------------------------------------------------------------------------------------

//Global game instance pointers
UMyGameInstance* g_gameInstance = nullptr;

//-----------------------------------------------------------------------------------------------------

UDataCollectionLibrary::UDataCollectionLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

//-----------------------------------------------------------------------------------------------------

void UDataCollectionLibrary::CollectEvent(FString eventType, TArray<FString> eventPayloadKeys, TArray<FString> eventPayloadValues)
{
	//Verify that the game instance is set
	if (g_gameInstance != nullptr)
	{
		//Store the event on the game instance
		g_gameInstance->StoreEvent(eventType, eventPayloadKeys, eventPayloadValues);
	}
}

//-----------------------------------------------------------------------------------------------------

void UDataCollectionLibrary::SetGameInstancePointer(UMyGameInstance* gameInstance)
{
	g_gameInstance = gameInstance;
}

//-----------------------------------------------------------------------------------------------------

void UDataCollectionLibrary::LogData()
{
	g_gameInstance->SaveDataToServerOrFile();
}

//-----------------------------------------------------------------------------------------------------

TArray<int> UDataCollectionLibrary::GetYearsFromNow()
{
	FDateTime now = FDateTime::Now();
	int currentYear = now.GetYear();

	TArray<int> years;

	while (currentYear >= 2017)
	{
		years.Add(currentYear);
		currentYear--;
	}

	return years;
}

//-----------------------------------------------------------------------------------------------------

FString UDataCollectionLibrary::GetTimeAsUTC(FDateTime dateTime)
{
	FDateTime utcNow = FDateTime::UtcNow();
	FDateTime localNow = FDateTime::Now();

	FTimespan timeDifference = utcNow - localNow;

	FDateTime utcTime = dateTime + timeDifference;

	FString utcString = utcTime.ToString();

	return utcString;
}