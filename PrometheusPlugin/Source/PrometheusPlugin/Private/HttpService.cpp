// Fill out your copyright notice in the Description page of Project Settings.

#include "HttpService.h"
#include "EngineUtils.h"
#include "PrometheusCameraBase.h"


// Sets default values
AHttpService::AHttpService(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//When the object is constructed, Get the HTTP module
	Http = &FHttpModule::Get();
}

// Called when the game starts or when spawned
void AHttpService::BeginPlay()
{
	//MyHttpCall();
	Super::BeginPlay();
}

/*Http call*/
void AHttpService::MyHttpCall()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::OnResponseReceived);
	//This is the url on which to process the request
	Request->SetURL("http://httpbin.org/ip");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

/*Assigned function on successfull http call*/
void AHttpService::OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	int32 responseCode = Response->GetResponseCode();
	FString printCode;
	printCode.AppendInt(responseCode);

	if (Response->GetResponseCode() == 200)
	{
		//handle the object in the response
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: BUTTS");
		
		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedInt = JsonObject->GetStringField("origin");

			//Output it to the engine
			//m_myPrometheusCamera->RecieveAppDataFromServer("PoopyPants");
			//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedInt);
		}
	}
	else
	{
		//print to a file
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: BUTTS");
	}


}

void AHttpService::RequestEventNames(FString dataToSend)
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::ReceiveEventNames);
	//This is the url on which to process the request
	Request->SetURL("http://httpbin.org/ip");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);
	Request->ProcessRequest();
}

void AHttpService::ReceiveEventNames(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	int32 responseCode = Response->GetResponseCode();
	FString printCode;
	printCode.AppendInt(responseCode);

	if (Response->GetResponseCode() == 200)
	{
		//handle the object in the response
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: Event names received from the server.");

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedInt = JsonObject->GetStringField("origin");

			//Output it to the engine
			//m_myPrometheusCamera->RecieveAppDataFromServer("PoopyPants");
			//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedInt);
		}
	}
	else
	{
		//print to a file
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: No database connection. Unable to receive event names.");
	}
}

void AHttpService::RequestEventData(FString dataToSend)
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::ReceiveEventData);
	//This is the url on which to process the request
	Request->SetURL("http://httpbin.org/ip");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);
	Request->ProcessRequest();
}

void AHttpService::ReceiveEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	int32 responseCode = Response->GetResponseCode();
	FString printCode;
	printCode.AppendInt(responseCode);

	if (Response->GetResponseCode() == 200)
	{
		//handle the object in the response
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: Event data received from the server.");

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedInt = JsonObject->GetStringField("origin");

			//Output it to the engine
			//m_myPrometheusCamera->RecieveAppDataFromServer("PoopyPants");
			//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedInt);
		}
	}
	else
	{
		//print to a file
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: No database connection. Unable to receive event names.");
	}


}

void AHttpService::SendEventData(FString dataToSend)
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::ValidateReceiptOfEventData);
	//This is the url on which to process the request
	Request->SetURL("http://httpbin.org/ip");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);
	Request->ProcessRequest();
}

void AHttpService::ValidateReceiptOfEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	int32 responseCode = Response->GetResponseCode();
	FString printCode;
	printCode.AppendInt(responseCode);

	if (Response->GetResponseCode() == 200)
	{
		//handle the object in the response
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: Data successfully sent to the database.");

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			//Get the value of the json object by field name
			FString recievedInt = JsonObject->GetStringField("origin");

			//Output it to the engine
			//m_myPrometheusCamera->RecieveAppDataFromServer("PoopyPants");
			//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, recievedInt);
		}
	}
	else
	{
		//print to a file
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange, "PROMETHEUS: No database connection, printing to file in Temp folder.");
	}


}