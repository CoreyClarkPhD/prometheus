// Fill out your copyright notice in the Description page of Project Settings.

#include "PrometheusCameraBase.h"
#include "EngineUtils.h"
#include "MyGameInstance.h"
#include "DataCollectionLibrary.h"

//-----------------------------------------------------------------------------------------------------

//	Get Events Json format
//
//	[
//		{
//			"ApplicationId": "LostInTheDark_16",
//			"RequestType" : "Events",
//			"EventType" : "",
//			"StartDate" : "",
//			"EndDate" : "",
//			"Options" : {}  //not used right now
//		}
//	]

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::MakeEventNameRequest()
{
	//needs to be the request from the server with appID
	FString dirPath = FString(FPaths::GameDir());
	FString gameName = FPaths::GetBaseFilename(dirPath);

	// create Json values array
	TArray< TSharedPtr<FJsonValue> > JsonArray;

	//create a blank Json object:
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	//Populate the Json object with the desired fields:
	JsonObject->SetStringField("ApplicationId", gameName);
	JsonObject->SetStringField("RequestType", "Data");
	JsonObject->SetStringField("EventType", "");
	JsonObject->SetStringField("StartDate", "");
	JsonObject->SetStringField("EndDate", "");

	//Add the Json object to the array:
	TSharedRef< FJsonValueObject > JsonValue = MakeShareable(new FJsonValueObject(JsonObject));
	JsonArray.Add(JsonValue);

	//Serialize the Json Array into OutputString:
	FString OutputString;
	TSharedRef<TJsonWriter<>> writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonArray, writer);

	//Execute the request
	RequestEventNames(OutputString);

	//Reset variables to aid the waiting screen on the UI
	m_waitingOnRequest = true;
	m_httpRequestTimer = 10.f;
}

//-----------------------------------------------------------------------------------------------------

//	Get Event Data Json
//
//	[
//		{
//			"ApplicationId": "LostInTheDark_16",
//			"RequestType" : "Data",
//			"EventType" : "Caroline Path",
//			"StartDate" : "2018.04.24-20.16.01",
//			"EndDate" : "2018.04.24-20.16.01",
//			"Options" : {}   //not used right now
//		}
//	]

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::MakeEventDataRequest(FString eventName, FString startDate, FString endDate)
{
	//needs to be the request from the server with appID
	FString dirPath = FString(FPaths::GameDir());
	FString gameName = FPaths::GetBaseFilename(dirPath);

	// create Json values array
	TArray< TSharedPtr<FJsonValue> > JsonArray;

	//create a blank Json object:
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

	//Populate the Json object with the desired fields:
	JsonObject->SetStringField("ApplicationId", gameName);
	JsonObject->SetStringField("RequestType", "Data");
	JsonObject->SetStringField("EventType", eventName);
	JsonObject->SetStringField("StartDate", startDate);
	JsonObject->SetStringField("EndDate", endDate);

	//Add the Json object to the array:
	TSharedRef< FJsonValueObject > JsonValue = MakeShareable(new FJsonValueObject(JsonObject));
	JsonArray.Add(JsonValue);

	//Serialize the Json Array into OutputString:
	FString OutputString;
	TSharedRef<TJsonWriter<>> writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonArray, writer);

	//Actually perform the http request
	RequestEventData(OutputString);

	//Reset variables to remove waiting screen from UI
	m_waitingOnRequest = true;
	m_httpRequestTimer = 10.f;
}

//-----------------------------------------------------------------------------------------------------

//Post Data Format
//[
//	{
//		"ApplicationId": "Your Application Name",
//		"EventType" : "Event1",
//		"EventDate" : "2018.04.24-20.16.01",
//		"EventPayload" :  //User Defined, event can be displayed because of WorldLocation
//		{
//			"WorldLocation": "X=-697.0 Y=-400.0 Z=72.15004",
//			"Variable 1" : "0.0",
//			"Variable 2" : "Some String"
//		}
//	},
//	{
//		"ApplicationId": "Your Application Name",
//		"EventType" : "Event2",
//		"EventDate" : "2018.04.24-20.16.01",
//		"EventPayload" :  //User Defined, event can not be displayed because no WorldLocation
//		{
//			"Variable A" : "Some String",
//			"Variable B" : "Another String"
//		}
//	}
//]

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::MakeDataPost(FString jsonString)
{
	//Pass json through the interface layer to the HTTP
	SendEventData(jsonString);
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::ReadInDatabaseInformation()
{
	//Get the path of the server information file
	FString dirPath = FString(FPaths::GameDir());
	FString loadDirectory = dirPath + "Plugins/PrometheusPlugin/Resources";
	FString fileName = "DatabaseInformation.json";

	//Make an empty Json string
	FString FullJsonString;

	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	// CreateDirectoryTree returns true if the destination
	// directory existed prior to call or has been created
	// during the call.
	if (PlatformFile.DirectoryExists(*loadDirectory))
	{
		// Get absolute file path
		FString AbsoluteFilePath = loadDirectory + "/" + fileName;

		// Allow overwriting or file doesn't already exist
		if (PlatformFile.FileExists(*AbsoluteFilePath))
		{
			//Read in the file to the empty string
			bool didLoad = FFileHelper::LoadFileToString(FullJsonString, *AbsoluteFilePath);
		}
	}

	//Extract the informatino we need from the json and set the database url member variable
	TSharedPtr<FJsonObject> databaseInformationJson;
	TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(FullJsonString);
	if (FJsonSerializer::Deserialize(JsonReader, databaseInformationJson))
	{
		m_databaseURL = databaseInformationJson->GetStringField("DatabaseURL");
	}
}

void APrometheusCameraBase::BeginPlay()
{
	//Initialize the FHttpModule
	Http = &FHttpModule::Get();

	//Initialize variables to aid the UI
	m_waitingOnRequest = false;
	DoneWaiting = false;
	WasLastCallData = false;
	WasRequestSuccessful = false;
	m_httpRequestTimer = 0.f;

	//Get database information
	ReadInDatabaseInformation();

	//Execute the blueprint beginplay
	Super::BeginPlay();

	
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::Tick(float DeltaSeconds)
{
	//not sure if this needs to be here or in the beginplay, but this works fine
	PrimaryActorTick.bCanEverTick = true;

	//Run the Blueprint tick function
	Super::Tick(DeltaSeconds);

	//Reset variables every frame (they're used in the blueprint code, then reset afterwards)
	DoneWaiting = false;
	WasLastCallData = false;
	WasRequestSuccessful = false;

	//If we're waiting on a request
	if (m_waitingOnRequest)
	{
		//Decrease the waiting timer
		m_httpRequestTimer -= DeltaSeconds;
		
		//If the timer ran out
		if (m_httpRequestTimer <= 0.f)
		{
			//Reset the timer and remove the Waiting screen (handled in blueprint using the bools)
			m_httpRequestTimer = 0.f;
			m_waitingOnRequest = false;
			DoneWaiting = true;
		}
	}
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::RequestEventNames(FString dataToSend)
{
	//Create blank request
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();

	//Bind the function that executes when the request returns from the server
	Request->OnProcessRequestComplete().BindUObject(this, &APrometheusCameraBase::ReceiveEventNames);

	//Set up request
	Request->SetURL(m_databaseURL);
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);

	//Send Request
	Request->ProcessRequest();
}

//-----------------------------------------------------------------------------------------------------

//	ReceiveEventNames Incoming Json Format
//
//	{
//		"EventNames": [
//			"Event1",
//			"Event2",
//			"Event3",
//			"Event4",
//			"Event5"
//		]
//	}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::ReceiveEventNames(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (Response.IsValid())
	{
		int responseCode = Response->GetResponseCode();
		if (responseCode == 200)
		{
			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			//Create a reader pointer to read the json data
			
			//!!!!!!!!This is the Real One!!!!!!!!!
			//TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Fake, just for testing
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create("{\"EventNames\":[\"Event1\",\"Event2\",\"Event3\",\"Event4\",\"Event5\"]}");
			
			//Deserialize the json data given Reader and the actual object
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				//TODO: comment
				TArray<TSharedPtr<FJsonValue>> eventNamesJson = JsonObject->GetArrayField("EventNames");

				TArray<FString> eventNamesStrings;

				for (int i = 0; i < eventNamesJson.Num(); i++)
				{
					FString eventName = eventNamesJson[i]->AsString();
					eventNamesStrings.Add(eventName);
				}

				//Use eventNamesStrings to populate Event Buffer
				g_gameInstance->ReadJsonStringIntoEventBuffer(eventNamesStrings);

				WasRequestSuccessful = true;

			}
		}
	}

	//Reset variables to remove the waiting screen on the UI
	m_waitingOnRequest = false;
	DoneWaiting = true;
	m_httpRequestTimer = 0.f;
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::RequestEventData(FString dataToSend)
{
	//Create blank request
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();

	//Bind the function that executes when the request returns from the server
	Request->OnProcessRequestComplete().BindUObject(this, &APrometheusCameraBase::ReceiveEventData);

	//Set up request
	Request->SetURL(m_databaseURL);
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);

	//Send Request
	Request->ProcessRequest();
}

//-----------------------------------------------------------------------------------------------------

//	ReceiveEventData Incoming Json Format
//
//[
//	{
//		"ApplicationId": "Your Application Name",
//		"EventType" : "Event1",
//		"EventDate" : "2018.04.24-20.16.01",
//		"EventPayload" :  //User Defined, event can be displayed because of WorldLocation
//		{
//			"WorldLocation": "X=-697.0 Y=-400.0 Z=72.15004",
//			"Variable 1" : "0.0",
//			"Variable 2" : "Some String"
//		}
//	},
//	{
//		"ApplicationId": "Your Application Name",
//		"EventType" : "Event2",
//		"EventDate" : "2018.04.24-20.16.01",
//		"EventPayload" :  //User Defined, event can not be displayed because no WorldLocation
//		{
//			"Variable A" : "Some String",
//			"Variable B" : "Another String"
//		}
//	}
//]

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::ReceiveEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (Response.IsValid())
	{
		if (Response->GetResponseCode() == 200)
		{
			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			g_gameInstance->m_loadedFileBuffer.Empty();
			
			//!!!!!!!!!!Real One!!!!!!!!!!!!
			//g_gameInstance->ReadJsonIntoOnlyDisplayBuffer(Response->GetContentAsString());

			//Testing
			FString TestingString = "[{\"ApplicationId\": \"LostInTheDark_16\",\"EventType\": \"Event1\",\"EventDate\": \"2018.04.24-20.16.01\",\"EventPayload\":{\"WorldLocation\": \"X=-697.0 Y=-400.0 Z=72.15004\",\"WorldDeltaSeconds\": \"0.0\",\"Butter\": \"Nut Squash\"}},{\"ApplicationId\": \"LostInTheDark_16\",\"EventType\": \"Event1\",\"EventDate\": \"2018.04.24-20.16.01\",\"EventPayload\":{\"WorldLocation\": \"X=-697.0 Y=-400.0 Z=72.15004\",\"WorldDeltaSeconds\": \"0.0\",\"Butter\": \"Nut Squash\"}},{\"ApplicationId\": \"LostInTheDark_16\",\"EventType\": \"Event1\",\"EventDate\": \"2018.04.24-20.16.01\",\"EventPayload\":{\"WorldLocation\": \"X=-697.0 Y=-400.0 Z=72.15004\",\"WorldDeltaSeconds\": \"0.0\",\"Butter\": \"Nut Squash\"}},{\"ApplicationId\": \"LostInTheDark_16\",\"EventType\": \"Event1\",\"EventDate\": \"2018.04.24-20.16.01\",\"EventPayload\":{\"WorldLocation\": \"X=-697.0 Y=-400.0 Z=72.15004\",\"WorldDeltaSeconds\": \"0.0\",\"Butter\": \"Nut Squash\"}}]";
			g_gameInstance->ReadJsonIntoOnlyDisplayBuffer(TestingString);

			WasLastCallData = true;
			WasRequestSuccessful = true;
		}
	}

	//Reset variables to remove the waiting screen on the UI
	m_waitingOnRequest = false;
	DoneWaiting = true;
	m_httpRequestTimer = 0.f;
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::SendEventData(FString dataToSend)
{
	//Create blank request
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();

	//Bind the function that executes when the request returns from the server
	Request->OnProcessRequestComplete().BindUObject(this, &APrometheusCameraBase::ValidateReceiptOfEventData);

	//Set up request
	Request->SetURL(m_databaseURL);
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetContentAsString(dataToSend);

	//Send Request
	Request->ProcessRequest();
}

//-----------------------------------------------------------------------------------------------------

void APrometheusCameraBase::ValidateReceiptOfEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (Response.IsValid())
	{
		if (Response->GetResponseCode() == 200)
		{
			//the data was received, so we can clear the data buffer on the game instance
			g_gameInstance->ClearEventBuffer();
		}
		else
		{
			//Data was not received correctly, dump to file
			g_gameInstance->PrintFileExecutable();
		}
	}
	else
	{
		//No valid response, dump to file
		g_gameInstance->PrintFileExecutable();
	}
}

//-----------------------------------------------------------------------------------------------------