// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "HttpService.h"
#include "PrometheusCameraBase.generated.h"

//-----------------------------------------------------------------------------------------------------

UCLASS()
class PROMETHEUSPLUGIN_API APrometheusCameraBase : public ACameraActor
{
	GENERATED_BODY()
	
public:
	//Variables for passing information up to the blueprint code (for waiting screen)
	UPROPERTY(BlueprintReadWrite)
	bool DoneWaiting;
	UPROPERTY(BlueprintReadWrite)
	bool WasRequestSuccessful;
	UPROPERTY(BlueprintReadWrite)
	bool WasLastCallData;

	//Variables for controlling the waiting screen in c++
	float m_httpRequestTimer;
	bool m_waitingOnRequest;

	//HTTP module to send/recieve http requests
	FHttpModule* Http;

	//The database URL is read in on beginplay
	FString m_databaseURL;

public:
	//Blueprint available functions for calling the http functions
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void MakeEventNameRequest();
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void MakeEventDataRequest(FString eventName, FString startDate, FString endDate);
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void MakeDataPost(FString jsonString);
	
	//Request the event names from the server send/recieve
	void RequestEventNames(FString dataToSend);
	void ReceiveEventNames(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	//Request the event specific data from the server send/recieve
	void RequestEventData(FString dataToSend);
	void ReceiveEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	//Post the playtest data to the server send/recieve
	void SendEventData(FString dataToSend);
	void ValidateReceiptOfEventData(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void ReadInDatabaseInformation();
};
