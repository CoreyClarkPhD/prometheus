// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "InputCoreTypes.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "MyGameInstance.generated.h"

USTRUCT()
struct FCustomEvent
{
	GENERATED_BODY()

	UPROPERTY()
	FString eventName;

	UPROPERTY()
	float timeOfEvent;

	UPROPERTY()
	FVector eventPosition;
};

USTRUCT()
struct FSmallEvent
{
	GENERATED_BODY()

	UPROPERTY()
	FString eventName;

	UPROPERTY()
	FVector eventPosition;
};

USTRUCT()
struct FNewCustomEvent
{
	GENERATED_BODY()

	UPROPERTY()
	FString m_applicationId;

	UPROPERTY()
	FString m_eventType;

	UPROPERTY()
	FString m_eventTime;

	UPROPERTY()
	TMap<FString, FString> m_eventValues;
};

USTRUCT()
struct FSuperNewCustomEvent
{
	GENERATED_BODY()

	UPROPERTY()
	FString ApplicationId;

	UPROPERTY()
	FString EventType;

	UPROPERTY()
	FString EventDate;

	UPROPERTY()
	TMap<FString, FString> m_eventValues;
};


USTRUCT()
struct FJsonArrayItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FString EventName;
	
	UPROPERTY()
	FString EventTime;

	UPROPERTY()
	FString EventPositionX;

	UPROPERTY()
	FString EventPositionY;

	UPROPERTY()
	FString EventPositionZ;
};


UCLASS()
class UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TArray<FCustomEvent> m_eventBuffer; //used temporarily to input files
	TArray<FNewCustomEvent> m_newEventBuffer; //used temporarily to input files
	TArray<FSmallEvent> m_loadedFileBuffer; //holds the entire loaded file
	TArray<FString> m_loadedEventNames; //holds just the names of the loaded events to populate the combobox

	FString m_currentDisplayedEvent;

public:
	UMyGameInstance();
	~UMyGameInstance();

	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	TArray<FString> GetLoadedEventNames();
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void SetCurrentDisplayedEvent(FString eventName);
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	TArray<FVector> GetEventLocations();
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void PrintFileExecutable();
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	void PrintFileAnytime();
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	bool GetListOfFilesAtTempLocation(TArray<FString>& Files);
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	bool NewReadJsonFileToDisplayBuffer(const FString& filename);
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	FString GenerateJsonStringFromCollectedData();

	void StoreEvent(FString eventType, TArray<FString> eventPayloadKeys, TArray<FString> eventPayloadValues);
	bool SaveStringToDataFile(const FString& SaveDirectory, const FString& FileName, const FString& TextToSave, bool AllowOverwriting);
	bool LoadDataFileToString(const FString& LoadDirectory, const FString& FileName, FString& StringToLoad);
	void ClearEventBuffer();
	bool IsRunningInExecutable();
	FString GetFilename();
	bool ReadInJsonStringToDisplayAndEventBuffers(const FString& jsonString);
	bool ReadJsonIntoOnlyDisplayBuffer(const FString& jsonString);
	bool ReadJsonStringIntoEventBuffer(const TArray<FString>& eventArray);
	void SaveDataToServerOrFile();
};
