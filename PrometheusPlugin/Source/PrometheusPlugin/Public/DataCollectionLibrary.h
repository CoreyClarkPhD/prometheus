// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DataCollectionLibrary.generated.h"

extern UMyGameInstance* g_gameInstance;

UCLASS()
class UDataCollectionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

//Blueprint Functions

	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	static void CollectEvent(FString eventType, TArray<FString> eventPayloadKeys, TArray<FString> eventPayloadValues);
	
	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	static void SetGameInstancePointer(UMyGameInstance* gameInstance);

	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	static void LogData();

	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	static TArray<int> GetYearsFromNow();

	UFUNCTION(BlueprintCallable, Category = "Prometheus")
	static FString GetTimeAsUTC(FDateTime dateTime);
};
